package main

import (
	"gitlab/shreychen/service-a/pkg/app"
	"log"
	"os"
	"time"

	"github.com/urfave/cli/v2"
)

var conf *app.Config

func init() {
	conf = app.NewConfig()
}

func main() {
	cliApp := &cli.App{
		Name:     "service-a",
		Version:  "v0.0.1",
		Compiled: time.Now(),
		Authors: []*cli.Author{
			{
				Name:  "Shiyuan Chen",
				Email: "sychen@gitlab.cn",
			},
		},
		Copyright:              "(c) 2021 JiHu GitLab",
		UseShortOptionHandling: true,
		Usage:                  "Service A",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "file",
				Aliases: []string{"f"},
				Usage:   "Load configuration from `FILE`",
				Value:   "config.yaml",
			},
		},
		Action: func(c *cli.Context) (err error) {
			err = conf.LoadFromFile(c.String("file"))
			app := app.NewApp(conf)
			app.Run()
			return
		},
	}

	err := cliApp.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
